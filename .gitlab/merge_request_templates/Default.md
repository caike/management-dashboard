## Semantic Versioning Checklist

Please make sure that your merge request adheres to Semantic Versioning guidelines before submitting it.

- [ ] This MR introduces a **breaking change**. (If yes, ensure version bump to `major`.)
- [ ] This MR adds a new **feature** in a backwards-compatible manner. (If yes, ensure version bump to `minor`.)
- [ ] This MR only includes **bug fixes or patches**. (If yes, ensure version bump to `patch`.)

### Use the appropriate title
- For breaking changes, use the prefix - `BREAKING CHANGE: <merge request title>`
- For features, use the prefix - `feat: <merge request title>`
- For bug fixes or patches, use the prefix - `fix: <merge request title>`

### Describe the changes

Provide a brief summary of the changes introduced by this merge request. Explain why they are considered a breaking change, a new feature, or a bug fix. Mention any specific semantic version numbers (e.g., `major`, `minor`, or `patch`) that you think should be applied after merging.

### Additional Information

Please add any relevant information or context about the changes made in this merge request.

### Checklist

- [ ] I have tested the changes thoroughly.
- [ ] The code follows the project's coding standards.
- [ ] Documentation has been updated or added, where necessary.
- [ ] All tests pass successfully.
- [ ] This MR is linked to any relevant issues or epics.
- [ ] Assign this MR to the appropriate reviewer(s).

By submitting this merge request, I acknowledge that I have followed the Semantic Versioning guidelines for the proposed changes.
