# Compute Provider Dashboard - CPD

## Installation

To set up the project, you need to have [Node.js](https://nodejs.org/en/download/) installed on your device. Then, navigate to the base directory of the project and run the following commands in your terminal:

```bash
npm install
```

```bash
npm run build
```

## Usage

After building the project, copy the `server.py` and `tests.py` files located in the project base directory into the newly generated `build` directory. Then, open a terminal in the `build` directory and run the `server.py` file.

Set up a wallet extension in the browser to make cryptocurrency transactions within the CPD. We tested with [Nami](https://docs.nunet.io/nunet-public-alpha-testnet/testing-configuration/nami-wallet-setup) and [Eternl](https://docs.nunet.io/nunet-public-alpha-testnet/testing-configuration/eternl-wallet-setup).

Refer to this section of the [README.md file](https://gitlab.com/nunet/management-dashboard/-/blob/develop/README.md#how-to-use-the-compute-provider-dashboard-user-interface) for information about how to use the application.

## Sequence Diagram

![Sequence Diagram](./sequence.png)


## License

Compute Provider Dashboard (CPD) is licensed under the [APACHE LICENSE, VERSION 2.0](https://www.apache.org/licenses/LICENSE-2.0.txt).

