import axios from "axios";
import { REQUEST_PATH, SEND_STATUS } from "../constants";

const ObjectFactory = (address: String, txHash: String) => {
  return {
    compute_provider_address: address,
    tx_hash: txHash,
    user_type: "CPD",
  };
};

export const getClaim = async (address: String, txHash: String, ip: String) => {
  const res = await axios.post(
    `${ip}${REQUEST_PATH}`,
    ObjectFactory(address, txHash)
  );
  console.log("CLAIM");
  return res.data;
};

export const getTransactions = async (ip: String, SizeDone = 20, CleanTx) => {
  const res = await axios.get(`${ip}/api/v1/transactions`, {
    params: {
      "size_done": SizeDone,
      "clean_tx": CleanTx
    }
  });
  return res.data;
};
export const sendStatus = async (body: any, ip: String) => {
  const res = await axios.post(`${ip}${SEND_STATUS}`, body);
  return res.data;
};

export const validateCaptcha = async (token: String) => {
  const res = await axios.post(
    "https://recaptcha.nunet.io/management-dashboard",
    {
      token,
    }
  );
  return res.data;
};
