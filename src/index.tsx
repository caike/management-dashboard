import React from "react";
import ReactDOM from "react-dom/client";
import "rsuite/dist/rsuite.min.css";
import "./index.scss";
import App from "./App";
import { MeshProvider } from "@meshsdk/react";
import "react-toastify/dist/ReactToastify.css";
import { ToastContainer } from "react-toastify";

const root = ReactDOM.createRoot(document.getElementById("root")!);
root.render(
  <MeshProvider>
    <App />
  </MeshProvider>
);
