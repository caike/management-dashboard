## [0.1.8-alpha.1](https://gitlab.com/nunet/management-dashboard/compare/v0.1.7...v0.1.8-alpha.1) (2023-10-24)


### Bug Fixes

* Configure nunet(non root) as user ([20eaca0](https://gitlab.com/nunet/management-dashboard/commit/20eaca094d917abd454d51ffeaf4782bb615e807))

<!-- New changes go on top. But below these comments. -->

<!-- We can track changes from Git logs, so why we needs this file?

Guiding Principles
- Changelogs are for humans, not machines.
- There should be an entry for every single version.
- The same types of changes should be grouped.
- Versions and sections should be linkable.
- The latest version comes first.
- The release date of each version is displayed.
- Mention whether you follow Semantic Versioning.

Types of changes
- `Added` for new features.
- `Changed` for changes in existing functionality.
- `Deprecated` for soon-to-be removed features.
- `Removed` for now removed features.
- `Fixed` for any bug fixes.
- `Security` in case of vulnerabilities.

-->

## [0.2.0](#14)
### Added
- transactions table 
- transactions operations
- filtering

## [0.1.7](#13)
### Added
- recaptcha error fixed
- unselect option added
- window sizing / margin fixed
- amounts to claim look weird (huge numbers) fixed

## [0.1.6](!43)
### Added

- Call to DMS /transactions to get list of hashes for jobs done
- Success/Fail message after withdrawal
### Changed

- TxHash included with request-reward and send-status
- Filter claimable tokens using txHashes from the DMS
- Appearance of claimable tokens table

## [0.1.5](https://gitlab.com/nunet/management-dashboard/-/tree/v0.1.5)

### Fixed
- Missing contract inputs
