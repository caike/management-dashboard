import { NotificationItem } from "../types/notificationItem";
import { useLocalStorage } from "./useLocalStorage";
import React, {
  JSXElementConstructor,
  ReactElement,
  ReactFragment,
  ReactPortal,
  createContext,
  useContext,
} from "react";

const Context = createContext<[NotificationItem[], Function]>([[], () => {}]);

export function NotificationProvider(props: {
  children:
    | string
    | number
    | boolean
    | ReactElement<any, string | JSXElementConstructor<any>>
    | ReactFragment
    | ReactPortal
    | null
    | undefined;
}) {
  const [notifications, setNotifications] = useLocalStorage(
    "notifications",
    []
  );
  return (
    <Context.Provider value={[notifications, setNotifications]}>
      {props.children}
    </Context.Provider>
  );
}

export function useNotificationsContext() {
  return useContext(Context);
}
