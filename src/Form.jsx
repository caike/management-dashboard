import React, { useEffect } from "react";
import { NunetParticles } from "./components/Particles";
import {
  Container,
  Header,
  Content,
  Form,
  ButtonToolbar,
  Button,
  Navbar,
  Panel,
  FlexboxGrid,
  Radio,
  Badge,
  RadioGroup,
  Whisper,
  Tooltip,
  IconButton,
  Nav,
  Modal,
  Loader,
  Message,
} from "rsuite";
import { UnLockAssetsInContract } from "tokens/UnlockContract";
import SendIcon from "@rsuite/icons/Send";
import CloseOutline from "@rsuite/icons/CloseOutline";
import CheckOutline from "@rsuite/icons/CheckOutline";
import InfoRoundIcon from "@rsuite/icons/InfoRound";
import NoticeIcon from "@rsuite/icons/Notice";
import { getClaim, sendStatus, getTransactions } from "./services/api";
import { useLocalStorage } from "./hooks/useLocalStorage";
import InfoOutline from "@rsuite/icons/InfoOutline";
import { useNotificationsContext } from "hooks/useNotifications";
import Logo from "./logo.png";

import { DMS } from "./constants";
import { Alert } from "./Alert/Alert";

import { useWallet, useAddress, CardanoWallet } from "@meshsdk/react";
import { IS_PRODUCTION } from "./settings";
import moment from "moment";
import { _getAllUserUtxos, _getDatum } from "./tokens/utils";
import UtxosTable from "components/Table";

export const AppForm = () => {
  const { wallet, connected } = useWallet();

  const [response, setResponse] = React.useState({});
  const [isLoading, setIsLoading] = React.useState(false);

  const [utxos, setUTXOs] = React.useState([]);

  const [datums, setDatums] = React.useState([]);

  const [txHashes, setTxHashes] = React.useState([]);

  const address = useAddress();

  const fetchTransactions = (SizeDone, CleanTx) => {
    getTransactions(ip, SizeDone, CleanTx)
      .then((x) => {
        if (x == null) {
          setTxHashes([]);
        } else {
          setTxHashes(x);
        }
      })
      .catch((e) => {
        setIsLoading(false);
        console.log(e);
        const error =
          e.response.data &&
          e.response.data.error &&
          e.response.data.error.length > 0
            ? e.response.data.error
            : "Can not load data. Please try again later.";
        setErrors([error]);
        setNotifications([
          {
            msg: error,
            isPositive: false,
            createdAt: Date.now(),
          },
          ...notifications.slice(0, 4),
        ]);
      });
  };

  useEffect(() => {
    fetchTransactions(20, "");
  }, []);

  const [form, setForm] = React.useState({
    blockChain: "Cardano",
  });

  const [open, setOpen] = React.useState(false);
  const [message, setMessage] = React.useState(null);
  const [reloadWindow, setReloadWindow] = React.useState(false);

  const handleOpen = () => setOpen(true);
  const handleClose = () => {
    setIsLoading(false);
    setOpen(false);
  };

  //const [ip, setIp] = useLocalStorage("ip", "http://localhost:9999");
  const ip = DMS;

  const populateUTXOs = async () => {
    let _addr = await wallet.getChangeAddress();
    console.log(_addr);
    let _utxos = await _getAllUserUtxos(_addr);
    let _filteredUtxos = [];
    _utxos.forEach((u) => {
      let hash = u.input.txHash;
      txHashes.forEach((t) => {
        if (hash === t.tx_hash || t.transaction_type === "done") {
          const isObjectWithTxHashExist = _filteredUtxos.some(
            (obj) => obj.tx_hash === t.tx_hash
          );
          if (!isObjectWithTxHashExist) {
            _filteredUtxos.push({
              ...u,
              timestamp: t.date_time,
              transaction_type: t.transaction_type,
              utxo: u,
              tx_hash: t.tx_hash,
            });
          }
        }
      });
    });
    let _datum = _getDatum(_filteredUtxos);
    console.log(_datum);
    console.log(_addr);

    setUTXOs(_filteredUtxos);
    setDatums(_datum);
  };

  useEffect(() => {
    if (connected) {
      populateUTXOs();
    }
  }, [wallet, connected, txHashes]);

  const [notifications, setNotifications] = useNotificationsContext();

  const [errors, setErrors] = React.useState([]);

  const triggerSendStatus = (status, reward_type, hash) => {
    sendStatus(
      {
        transaction_status: status,
        transaction_type: reward_type,
        tx_hash: hash,
      },
      ip
    ).then((x) => console.log(x));
    setIsLoading(false);
  };

  const [walletOperationInfo, setWalletOperationInfo] = React.useState({});

  const walletJob = async () => {
    // ('Fund, 0), ('Withdraw, 1), ('Refund, 2), ('Distribute, 3)
    const EscrowRedeemer = {
      fund: 0,
      withdraw: 1,
      refund: 2,
      distribute: 3,
    };
    await UnLockAssetsInContract(
      wallet,
      address,
      walletOperationInfo.utxo,
      response.service_provider_addr,
      response.reward_type,
      response.signature_datum,
      response.message_hash_datum,
      response.datum,
      response.signature_action,
      response.message_hash_action,
      response.action
    )
      .then((x) => {
        triggerSendStatus(
          "success",
          response.reward_type,
          walletOperationInfo.hash
        );
        console.log(x);
        setReloadWindow(true);

        setMessage(<span style={{ fontSize: "1.5rem" }}>Success</span>);
        setOpen(true);
      })
      .catch((e) => {
        console.log("EXCEPTION:", e);
        setMessage(
          <>
            <div style={{ fontSize: "1.5rem" }}>
              Error: Something went wrong
            </div>
            <br />
            <div
              style={{
                background: "#1e1e1e",
                fontFamily: "monospace",
                width: "90%",
                margin: "2rem auto",
                borderRadius: "5px",
                color: "wheat",
                fontSize: "1.2rem",
                padding: "2rem",
              }}
            >
              {e.message}
            </div>
          </>
        );
        setReloadWindow(true);

        setOpen(true);
        triggerSendStatus(
          "error",
          response.reward_type,
          walletOperationInfo.hash
        );
      });
  };

  const onSubmit = async (hash, utxo) => {
    setErrors([]);
    setIsLoading(true);
    setWalletOperationInfo({ hash, utxo });

    const collectedErrors = [];

    if (collectedErrors.length > 0 && IS_PRODUCTION) {
      setErrors([...collectedErrors]);
      setNotifications([
        {
          msg: collectedErrors[0],
          isPositive: false,
          createdAt: Date.now(),
        },
        ...notifications.slice(0, 4),
      ]);
      window.scrollTo({ top: 0, left: 0, behavior: "smooth" });
    } else {
      await getClaim(address, hash, ip)
        .then((x) => {
          setResponse(x);
          setMessage(
            <span style={{ fontSize: "1.5rem" }}>
              we will be using transaction type <code>{x.reward_type}</code>
            </span>
          );
          setOpen(true);
        })
        .catch((e) => {
          setIsLoading(false);

          console.log(e);
          const error = e.response.data.error;
          setErrors([error]);
          setNotifications([
            {
              msg: error,
              isPositive: false,
              createdAt: Date.now(),
            },
            ...notifications.slice(0, 4),
          ]);
        });
    }
  };

  return (
    // <NunetLoader duration={2000} onFinsih={() => {}}>
    <div className="show-fake-browser login-page">
      <Modal
        backdrop="static"
        open={open}
        onClose={handleClose}
        size="sm"
        style={{ marginTop: "3rem" }}
      >
        <Modal.Body
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            flexDirection: "column",
          }}
        >
          <InfoOutline
            style={{ color: "blue", fontSize: 24, marginRight: "0.5rem" }}
          />
          {message}
          {/* <span style={{ fontSize: "1.5rem" }}>
            we will be using transaction type{" "}
            <code>{response.reward_type}</code>
          </span> */}
        </Modal.Body>
        <Modal.Footer>
          <Button
            onClick={() => {
              walletJob();
              if (reloadWindow) {
                window.location.reload();
              }
              handleClose();
            }}
            appearance="primary"
          >
            Ok
          </Button>
          {!reloadWindow && (
            <Button onClick={handleClose} appearance="subtle">
              Cancel
            </Button>
          )}
        </Modal.Footer>
      </Modal>
      <Container>
        <Header style={{ zIndex: "99999" }}>
          <Navbar appearance="inverse" style={{ background: "#0a2042b9" }}>
            <Navbar.Brand style={{ textAlign: "left" }}>
              <span style={{ color: "#fff" }}>
                <img
                  src={Logo}
                  alt="logo"
                  style={{
                    width: "100px",
                    marginTop: "-5px",
                    textAlign: "left",
                  }}
                />
              </span>
            </Navbar.Brand>
            <Nav>
              <Nav.Menu
                className="darkMenu"
                icon={<NoticeIcon />}
                title={<Badge content={notifications.length} />}
              >
                {notifications.map((n, index) => (
                  <Nav.Item
                    key={index}
                    className="darkMenu_item"
                    style={{
                      borderBottom: "0.1px solid #444",
                    }}
                  >
                    {n.isPositive ? (
                      <CheckOutline color="#4cd137" />
                    ) : (
                      <CloseOutline color="#e84118" />
                    )}
                    {n.msg}
                    <Form.HelpText
                      style={{ fontSize: "0.6rem", textAlign: "right" }}
                    >
                      {moment(n.createdAt).format("MMMM Do YYYY, h:mm:ss a")}
                    </Form.HelpText>
                  </Nav.Item>
                ))}
                {notifications.length > 0 && (
                  <Nav.Item
                    className="darkMenu_item"
                    onClick={() => setNotifications([])}
                    style={{
                      textAlign: "center",
                      color: "#d63031",
                      fontWeight: "bold",
                    }}
                  >
                    Clear Notifications
                  </Nav.Item>
                )}
              </Nav.Menu>
            </Nav>
            <Nav pullRight className="CardanoWallet">
              <Whisper
                trigger="hover"
                placement="left"
                speaker={
                  <Tooltip style={{ zIndex: "9999999" }}>
                    Please note that this balance is your total balance with
                    amount on hold
                  </Tooltip>
                }
              >
                <span style={{ zIndex: "9999999" }}>
                  <CardanoWallet style={{ zIndex: "9999999" }} />
                </span>
              </Whisper>
            </Nav>
          </Navbar>
        </Header>
        {errors.length > 0 && <Alert text={errors[0]} />}
        {/* HERE! */}
        <NunetParticles />
        <Content>
          <FlexboxGrid justify="center" style={{ margin: "2rem 0" }}>
            <FlexboxGrid.Item colspan={12} className="flex-panel-container">
              <Panel
                style={{ background: "white", padding: "10px" }}
                className="flex-panel"
                header={
                  <h4
                    style={{
                      textAlign: "center",
                      fontWeight: "300",
                      marginBottom: "15px",
                      position: "relative",
                      fontSize: "30px",
                    }}
                  >
                    Request NTX Tokens 🪙
                    <hr />
                  </h4>
                }
                bordered
                shaded
              >
                <Form fluid>
                  <Form.Group name="blockchain" label="Blockchain">
                    <Form.ControlLabel style={{ position: "relative" }}>
                      Blockchain:
                      <Whisper
                        trigger="hover"
                        placement="left"
                        speaker={
                          <Tooltip>
                            The blockchain needed, Etherum is still in
                            development
                          </Tooltip>
                        }
                      >
                        <IconButton
                          size="xs"
                          circle
                          style={{
                            position: "absolute",
                            right: "0",
                            top: "-3px",
                          }}
                          icon={<InfoRoundIcon color="#78909b" />}
                        />
                      </Whisper>
                    </Form.ControlLabel>
                    <RadioGroup
                      name="blockchain"
                      color="#0A2042"
                      inline
                      value={form.blockChain}
                      onChange={(v) => setForm({ ...form, blockChain: v })}
                    >
                      <Radio value={"Cardano"}>Cardano</Radio>
                      <Radio disabled={true} value={"Ethereum"}>
                        Ethereum (In Development)
                      </Radio>
                    </RadioGroup>
                  </Form.Group>

                  <Form.Group>
                    {datums.length > 0 ? (
                      <UtxosTable
                        data={datums}
                        handleSend={onSubmit}
                        fetchTransactions={fetchTransactions}
                      ></UtxosTable>
                    ) : (
                      <Message>Please Connect your wallet.</Message>
                    )}
                    {/* 
                    <ButtonToolbar>
                      <Button
                        appearance="primary"
                        size="lg"
                        className="gr-btn"
                        style={{ width: "100%", marginTop: "1rem" }}
                        disabled={
                          (selectedRowIndex == null ? true : false) ||
                          !connected
                        }
                        onClick={onSubmit}
                      >
                        {isLoading ? (
                          <Loader />
                        ) : (
                          <>
                            <SendIcon /> Request
                          </>
                        )}
                      </Button>
                      {!connected && IS_PRODUCTION && (
                        <Form.HelpText>
                          You have to connect a wallet to submit.
                        </Form.HelpText>
                      )}
                    </ButtonToolbar> */}
                  </Form.Group>
                </Form>
              </Panel>
            </FlexboxGrid.Item>
          </FlexboxGrid>
        </Content>
      </Container>
    </div>
    // </NunetLoader>
  );
};
