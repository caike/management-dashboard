import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { AppProvider } from "./store/provider";
import { AppForm } from "./Form";
import { Page404 } from "./pages/Page404";
import { CAPTCHA_SITE_KEY } from "./constants";
import { NotificationProvider } from "hooks/useNotifications";

function App() {
  return (
    <AppProvider>
      <NotificationProvider>
        <BrowserRouter>
          <Routes>
            {/* <Route path="/" element={<SocketContainer />} /> */}
            <Route path="/" element={<AppForm />} />
            {/* <Route path="/dms" element={<NetwrokChanger />} /> */}
            <Route path="/*" element={<Page404 />} />
          </Routes>
        </BrowserRouter>
      </NotificationProvider>
    </AppProvider>
  );
}

export default App;
