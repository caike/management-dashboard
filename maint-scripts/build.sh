#!/bin/bash

APP_VERSION=$(cat package.json | jq -r .version)
OUTPUT_DIR=dist

PROJECT_ROOT=$(pwd)
JS_BUILD_DIR=build
DEBIAN_BUILD_DIR=$PROJECT_ROOT/maint-scripts/management-dashboard

while getopts 'v:o:h' opt; do
  case "$opt" in
    v)
      echo "Setting version as ${OPTARG}"
      APP_VERSION=${OPTARG}
      ;;
    o)
      echo "Outputting artifact in ${OPTARG}"
      OUTPUT_DIR=${OPTARG}
      ;;
    ?|h)
      echo "Usage: $(basename $0) [-v arg] [-o arg]"
      exit 1
      ;;
  esac
done

shift "$(($OPTIND -1))"

if ! command -v jq &>/dev/null ; then  apt update && apt install -y jq; fi

mkdir -p $OUTPUT_DIR

npm install --save --legacy-peer-deps && npm run build

cp -r $JS_BUILD_DIR/* $DEBIAN_BUILD_DIR/var/www/management-dashboard

APP_INST_SIZE=$(du -sB1 $DEBIAN_BUILD_DIR | awk '{ print $1 }') 

cp $DEBIAN_BUILD_DIR/DEBIAN/control /tmp/debian_control

sed -i "s/Version:.*/Version: $APP_VERSION/g" /tmp/debian_control

sed -i "s/Installed-Size:.*/Installed-Size: $APP_INST_SIZE/g" /tmp/debian_control

cp /tmp/debian_control $DEBIAN_BUILD_DIR/DEBIAN/control

chmod -R 0755 $DEBIAN_BUILD_DIR

dpkg-deb --build $DEBIAN_BUILD_DIR $OUTPUT_DIR