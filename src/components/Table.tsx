import React, { useState } from "react";
import { Table, Button } from "rsuite";
import { convertDateFormat } from "../tokens/utils";
import Check from "@rsuite/icons/Check";
import { TableTabs } from "./TableTabs";
import { capitalizeFirstLetter } from "../helpers/Capitalize";

const { Column, HeaderCell, Cell } = Table;

function UtxosTable(props) {
  const [active, setActive] = useState("done");
  const [desiredIndex, setDesiredIndex] = useState<any>(null);
  const data = props.data;

  const [sortColumn, setSortColumn] = React.useState<any>();
  const [sortType, setSortType] = React.useState<any>();
  const [loading, setLoading] = React.useState<any>(false);

  const handleSortColumn = (sortColumn, sortType) => {
    setLoading(true);
    setTimeout(() => {
      setLoading(false);
      setSortColumn(sortColumn);
      setSortType(sortType);
    }, 200);
  };

  const sortData = (d) => {
    if (sortColumn && sortType) {
      return d.sort((a, b) => {
        let x = a[sortColumn];
        let y = b[sortColumn];
        if (typeof x === "string") {
          x = x.charCodeAt(0);
        }
        if (typeof y === "string") {
          y = y.charCodeAt(0);
        }
        if (sortType === "asc") {
          return x - y;
        } else {
          return y - x;
        }
      });
    }
    return d;
  };

  function truncateString(str, maxLength) {
    if (str.length > maxLength) {
      return str.substring(0, maxLength) + "...";
    }
    return str;
  }
  return (
    <>
      <TableTabs
        appearance="tabs"
        active={active}
        onSelect={setActive}
        data={data}
      />
      <Table
        bordered
        cellBordered
        sortColumn={sortColumn}
        sortType={sortType}
        onSortColumn={handleSortColumn}
        loading={loading}
        data={sortData(
          data
            .map((row, index) => ({
              id: index,
              from: truncateString(row.fields[0], 10),
              amount: row.fields[6],
              date: convertDateFormat(row.timestamp),
              transaction_type: row.transaction_type,
              tx_hash: row.tx_hash,
              utxo: row.utxo,
            }))
            .filter((row) => row.transaction_type.startsWith(active))
            .map((row, index) => ({ count: index + 1, ...row }))
        )}
      >
        <Column flexGrow={1} sortable>
          <HeaderCell>#</HeaderCell>
          <Cell dataKey="count" />
        </Column>
        <Column flexGrow={2} sortable>
          <HeaderCell>From</HeaderCell>
          <Cell dataKey="from" />
        </Column>

        <Column flexGrow={1} sortable>
          <HeaderCell>Amount</HeaderCell>
          <Cell dataKey="amount" />
        </Column>

        <Column flexGrow={2} sortable>
          <HeaderCell>Date</HeaderCell>
          <Cell dataKey="date" />
        </Column>

        <Column>
          <HeaderCell>...</HeaderCell>

          <Cell style={{ padding: "6px" }}>
            {(rowData) => (
              <>
                {active === "distribute" ||
                active === "distribute-50" ||
                active === "distribute-75" ||
                active === "withdraw" ? (
                  <Button
                    size="xs"
                    appearance="primary"
                    onClick={() => {
                      props.handleSend(
                        data[+rowData.id].tx_hash,
                        data[+rowData.id].utxo
                      );
                      setDesiredIndex(+rowData.id as any);
                    }}
                  >
                    {capitalizeFirstLetter(active)}
                  </Button>
                ) : (
                  <span>{capitalizeFirstLetter(active)}</span>
                )}
              </>
            )}
          </Cell>
        </Column>
      </Table>
      <div style={{ textAlign: "right", padding: "1rem" }}>
        <Button
          size="sm"
          color="red"
          appearance="primary"
          disabled={
            data.filter((d) => d.transaction_type.startsWith(active)).length ===
              0 ||
            (active !== "done" && active !== "refund")
          }
          onClick={() => props.fetchTransactions(20, active)}
        >
          Clear
        </Button>
      </div>{" "}
    </>
  );
}

export default UtxosTable;
