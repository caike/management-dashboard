# What is the Compute Provider Dashboard?

The Compute Provider Dashboard (CPD) is a locally available interface that enables compute providers to
monitor and manage their machines effectively. The current version of the dashboard facilitates receiving
NTX tokens for computational jobs. In future, it would provide basic telemetry data, such as resource
utilization, job status, and system performance, allowing providers to optimize their services.
The dashboard interface aims to offer valuable insights into token management, machine performance and
resource allocation. It would empower providers to make informed decisions about their services and ensure
the best possible experience for their users.

Note: If you are a developer, please check out [these instructions](https://gitlab.com/nunet/management-dashboard/-/blob/develop/README-DEV.md).

# How to Install the Compute Provider Dashboard?

## Prerequisites:

Before you install the Compute Provider Dashboard, make sure:
- you have already installed the device management service
- you are using a desktop machine (servers are not currently supported)

## Installation

To install the Compute Provider Dashboard on your system, follow these steps:

Step 1: Download the nunet-cpd-latest.deb package using the wget command. Open a terminal
window and enter the following command:
```
wget https://d.nunet.io/nunet-cpd-latest.deb -O nunet-cpd-latest.deb
```
This command will download the nunet-cpd-latest.deb package from the provided URL.

Step 2: Install the nunet-cpd-latest.deb package. After downloading the package, enter the following
command in the terminal:

```
sudo apt update && sudo apt install ./nunet-cpd-latest.deb -y
```

This command will update your system's package index and then install the nunet-cpd-latest.deb
package. The -y flag automatically accepts the installation prompts.

Step 3: Access the compute provider dashboard. Open your preferred internet browser and visit:

```
localhost:9992
```
This URL will direct you to the Compute Provider Dashboard hosted on your local machine, where you can
start using the service to manage your machine learning jobs and connect your NTX wallet.

If you wish to either change the port or allow the dashboard to be accessed from outside localhost, then edit the file under _/etc/systemd/system/management-dashboard.service_ - changes to this file require subsequent `sudo systemctl daemon-reload` and `sudo systemctl restart management-dashboard` commands.

Keep in mind that these installation instructions assume you are using a Debian-based Linux distribution,
such as Ubuntu. The installation process may differ for other operating systems.

# How to Use the Compute Provider Dashboard User Interface?

To use the device management service with the Cardano blockchain, follow these steps to connect your
NTX wallet and integrate it with the management dashboard:

**Step 1**: Add your NTX wallet address, which is based on the Cardano blockchain. Your wallet address is a
unique identifier that enables you to receive and send NTX tokens within the Cardano network.

**Step 2**: Select the Cardano blockchain by clicking on the corresponding radio button. This ensures that the
device management service communicates with the appropriate blockchain network to facilitate the
exchange of NTX tokens for machine learning job requests.

**Step 3**: Connect your wallet to the Compute Provider Dashboard by clicking the "Connect Wallet" button at
the top right corner of your browser. This action will initiate a secure connection between your wallet and the
dashboard, allowing for seamless token transactions.

Finally, click on "Submit" to confirm your wallet connection and complete the integration process. Once
connected, you can use your NTX tokens to request resources and manage your machine learning jobs
through the Compute Provider Dashboard.

# Understanding Issues on the Compute Provider Dashboard

If you experience issues while using the dashboard, you can open the inspect console in your browser to
get more information about the error. Here's how to do it:

1. Open the dashboard in your web browser.
2. Right-click anywhere on the page and select "Inspect" from the context menu. Alternatively, you can use
the keyboard shortcut Ctrl + Shift + I on Windows/Linux or Cmd + Option + I on Mac to
open the inspect console.
3. The inspect console will open in a separate window or panel. Look for the "Console" tab, which should
be near the top of the panel.
4. If there are any errors, they will be displayed in the console with a red message.

When you hit the `/run/request-reward` endpoint, you may encounter the following errors:
404 Not Found: This error occurs when there is no active job to claim.
102 Processing: This error occurs when there is an active job but it has not finished yet. The user
should wait until they request again.
500 Internal Server Error: This error occurs when the connection to the Oracle fails.
200 OK: This response indicates success. It includes the `signature` , `oracle_message` , and
`reward_type`.

When you hit the `/run/send-status endpoint`, you may encounter the following errors:
400 Bad Request: This error occurs when the payload is not structured properly and cannot be read.
200 OK: This response indicates success. It includes the message "transaction status %s
acknowledged", where `%s` is one of the transaction status sent from the webapp.

# License

Compute Provider Dashboard (CPD) is licensed under the [APACHE LICENSE, VERSION 2.0](https://www.apache.org/licenses/LICENSE-2.0.txt).

