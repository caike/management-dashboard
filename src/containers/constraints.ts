const dict = {
  Low: {
    CPU: 500,
    RAM: 2000,
    VRAM: 2000,
    power: 170,
    complexity: "Low",
  },
  Moderate: {
    CPU: 1500,
    RAM: 8000,
    VRAM: 8000,
    power: 220,
    complexity: "Moderate",
  },
  High: {
    CPU: 2500,
    RAM: 16000,
    VRAM: 24000,
    power: 350,
    complexity: "High",
  },
};

export const constraintsMapper = (l) => dict[l];
