export const TENSORFLOW_REGISTRY =
  "registry.gitlab.com/nunet/ml-on-gpu/ml-on-gpu-service/develop/tensorflow";

export const PYTORCH_REGISTRY =
  "registry.gitlab.com/nunet/ml-on-gpu/ml-on-gpu-service/develop/pytorch";

export const SERVICE_TYPE = "ml-training-gpu";

export const SCRIPT_ADDRESS =
  "addr_test1wplx9dwzmn986k48kwmqn75yjlhlwcy094euq8c7s2ws8xc5k5uu6";

export const DMS = "http://localhost:9999";
export const REQUEST_PATH = "/api/v1/transactions/request-reward";
export const BACKEND_ENDPOINT = `${DMS}${REQUEST_PATH}`;
export const CAPTCHA_SITE_KEY = "6Lfb-LslAAAAAFsG38WuUfkj7yifiiEr9gtucSiq";
export const SEND_STATUS = "/api/v1/run/send-status";
